from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse('<h1>This is Articles App!</h1>')

def homepage(request):
    return HttpResponse('<h1>Главная страница</h1>')

# [id, author, year, article_name, [tags]]
articles_list = [
    [1, "Серега", 2019, "Учиться, учиться, учиться", ["образование","технологии"]],
    [2, "Егор", 2019, "О море", ["путешествия"]],
    [3, "Ирина", 2019, "Как я провела лето", ["путешествия","образование"]],
    [4, "Марина", 2019, "Всё, что вам нужно знать о Django", ["образование","технологии"]],
    [5, "Стас", 2019, "10 цитат А.С. Пушкина", ["творчество"]],
    [6, "Серега", 2018, "Мой путь в IT", ["образование","технологии"]],
    [7, "Егор", 2018, "Страны которые могут исчезнуть в течении 50 лет", ["путешествия"]],
    [8, "Ирина", 2017, "Британские учёные", ["технологии"]],
    [9, "Марина", 2017, "Разработка на Django", ["технологии"]],
    [10, "Стас", 2016, "Книги, которые должен прочитать каждый", ["творчество"]],
    [11, "Стас", 2016, "Путеводитель по русской поэзии", ["творчество"]]
]

def generate_html(articles):
    if len(articles) == 0:
        return '<h1>По вашему запросу не найдено ни одной статьи!</h1>'
    else:
        base_html = '<h1>Статьи по вашему запросу:</h1> <ul>'
        for article in articles:
            list_item = f'<li><ul><pre><li>{article[3]}</li><li>автор: {article[1]}</li><li>тэги: {" ".join(article[4])}</li><li>год: {article[2]}</li></pre></ul></li>'
            base_html += list_item
        base_html += '</ul>'
        return base_html

def dashboard(request, year=2019):
    found_articles = []
    for article in articles_list:
        if year == article[2]:
            found_articles.append(article)

    return HttpResponse(generate_html(found_articles))

def article_by_id(request, id):
    found_articles = []
    for article in articles_list:
        if id == article[0]:
            found_articles.append(article)

    return HttpResponse(generate_html(found_articles))

def articles_by_user(request, user):
    found_articles = []
    for article in articles_list:
        if user == article[1]:
            found_articles.append(article)

    return HttpResponse(generate_html(found_articles))


def articles_by_tag(request, tag):
    found_articles = []
    for article in articles_list:
        if tag in article[4]:
            found_articles.append(article)

    return HttpResponse(generate_html(found_articles))
