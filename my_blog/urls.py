from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('articles/', include('articles.urls')),
    path('accounts/', include('accounts.urls')),
    path('admin/', admin.site.urls),
]
