from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse('<h1>Добро пожаловать!</h1><p>Вы можете попробовать ввести в адресную строку следующие пути:</p><ul><pre><li> articles<ul><li>/dashboard - отображает статьи за текущий год</li><li>/dashboard/int:year - отображает статьи за указанный год </li><li>/int:id - отображает статью с указанным id</li><li>/str:user - отображает статьи указанного пользователя</li><li>/tag/str:tag - отображает статьи с указанным тэгом</li></ul></li><li> accounts<ul><li>/friends/str:user - отображает друзей указанного пользователя</li></ul></li></pre></ul>')

