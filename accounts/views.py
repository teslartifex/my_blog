from django.shortcuts import render
from django.http import HttpResponse

friends_dict = {
    'Серега' : ['Егор', 'Марина'],
    'Егор' : ['Серега', 'Ирина'],
    'Марина' : ['Ирина'],
    'Ирина' : ['Серега', 'Егор', 'Марина'],
    'Стас' : []
}

def friends(request, user):
    friends_count = len(friends_dict[user])
    if friends_count == 1:
        friends = friends_dict[user]
        return HttpResponse(f'<h1>У пользователя {user} один друг: {friends[0]}</h1>')
    elif friends_count > 0:
        friends = friends_dict[user]
        friends_string = ' '.join(friends)
        return HttpResponse(f'<h1>У пользователя {user} {friends_count} друга: {friends_string}</h1>') 
    else:
        return HttpResponse(f'<h1>У пользователя {user} нет друзей :(</h1>')    

def index(request):
    return HttpResponse('<h1>This is Accounts App!</h1>')

def sign_up(request):
    return HttpResponse('<h1>Регистрация</h1>')
def sign_in(request):
    return HttpResponse('<h1>Логин</h1>')
def my_account(request):
    return HttpResponse('<h1>Мой аккаунт</h1>')
